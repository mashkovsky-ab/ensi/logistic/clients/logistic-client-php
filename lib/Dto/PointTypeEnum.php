<?php
/**
 * PointTypeEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\LogisticClient\Dto;
use \Ensi\LogisticClient\ObjectSerializer;

/**
 * PointTypeEnum Class Doc Comment
 *
 * @category Class
 * @description Тип точки выдачи заказов. Расшифровка значений:   * &#x60;1&#x60; - Пункт выдачи заказа   * &#x60;2&#x60; - Постамат   * &#x60;3&#x60; - Отделение почты России   * &#x60;4&#x60; - Терминал
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PointTypeEnum
{
    /**
     * Possible values of this enum
     */
    const PICKUP_POINT = 1;
    const POSTOMAT = 2;
    const RU_POST_OFFICE = 3;
    const TERMINAL = 4;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::PICKUP_POINT,
            self::POSTOMAT,
            self::RU_POST_OFFICE,
            self::TERMINAL,
        ];
    }
}


