<?php
/**
 * ShipmentMethodEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\LogisticClient\Dto;
use \Ensi\LogisticClient\ObjectSerializer;

/**
 * ShipmentMethodEnum Class Doc Comment
 *
 * @category Class
 * @description Способы доставки на нулевой миле (доставка от продавца до распределительного центра). Расшифровка значений:   * &#x60;1&#x60; - Курьер службы доставки   * &#x60;2&#x60; - Собственный курьер
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ShipmentMethodEnum
{
    /**
     * Possible values of this enum
     */
    const DS_COURIER = 1;
    const OWN_COURIER = 2;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::DS_COURIER,
            self::OWN_COURIER,
        ];
    }
}


