<?php
/**
 * CargoReadonlyProperties
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\LogisticClient\Dto;

use \ArrayAccess;
use \Ensi\LogisticClient\ObjectSerializer;

/**
 * CargoReadonlyProperties Class Doc Comment
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CargoReadonlyProperties implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'CargoReadonlyProperties';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'seller_id' => 'int',
        'store_id' => 'int',
        'delivery_service_id' => 'int',
        'created_at' => '\DateTime',
        'updated_at' => '\DateTime',
        'status_at' => '\DateTime',
        'is_problem_at' => '\DateTime',
        'is_canceled_at' => '\DateTime',
        'error_xml_id' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => null,
        'seller_id' => null,
        'store_id' => null,
        'delivery_service_id' => null,
        'created_at' => 'date-time',
        'updated_at' => 'date-time',
        'status_at' => 'date-time',
        'is_problem_at' => 'date-time',
        'is_canceled_at' => 'date-time',
        'error_xml_id' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'id' => false,
        'seller_id' => false,
        'store_id' => false,
        'delivery_service_id' => false,
        'created_at' => false,
        'updated_at' => false,
        'status_at' => false,
        'is_problem_at' => false,
        'is_canceled_at' => false,
        'error_xml_id' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::$openAPINullables[$property])) {
            return self::$openAPINullables[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'seller_id' => 'seller_id',
        'store_id' => 'store_id',
        'delivery_service_id' => 'delivery_service_id',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'status_at' => 'status_at',
        'is_problem_at' => 'is_problem_at',
        'is_canceled_at' => 'is_canceled_at',
        'error_xml_id' => 'error_xml_id'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'seller_id' => 'setSellerId',
        'store_id' => 'setStoreId',
        'delivery_service_id' => 'setDeliveryServiceId',
        'created_at' => 'setCreatedAt',
        'updated_at' => 'setUpdatedAt',
        'status_at' => 'setStatusAt',
        'is_problem_at' => 'setIsProblemAt',
        'is_canceled_at' => 'setIsCanceledAt',
        'error_xml_id' => 'setErrorXmlId'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'seller_id' => 'getSellerId',
        'store_id' => 'getStoreId',
        'delivery_service_id' => 'getDeliveryServiceId',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt',
        'status_at' => 'getStatusAt',
        'is_problem_at' => 'getIsProblemAt',
        'is_canceled_at' => 'getIsCanceledAt',
        'error_xml_id' => 'getErrorXmlId'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data, null);
        $this->setIfExists('seller_id', $data, null);
        $this->setIfExists('store_id', $data, null);
        $this->setIfExists('delivery_service_id', $data, null);
        $this->setIfExists('created_at', $data, null);
        $this->setIfExists('updated_at', $data, null);
        $this->setIfExists('status_at', $data, null);
        $this->setIfExists('is_problem_at', $data, null);
        $this->setIfExists('is_canceled_at', $data, null);
        $this->setIfExists('error_xml_id', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return $this
     */
    public function setId($id)
    {


        /*if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }*/
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets seller_id
     *
     * @return int|null
     */
    public function getSellerId()
    {
        return $this->container['seller_id'];
    }

    /**
     * Sets seller_id
     *
     * @param int|null $seller_id Идентификатор продавца
     *
     * @return $this
     */
    public function setSellerId($seller_id)
    {


        /*if (is_null($seller_id)) {
            throw new \InvalidArgumentException('non-nullable seller_id cannot be null');
        }*/
        $this->container['seller_id'] = $seller_id;

        return $this;
    }

    /**
     * Gets store_id
     *
     * @return int|null
     */
    public function getStoreId()
    {
        return $this->container['store_id'];
    }

    /**
     * Sets store_id
     *
     * @param int|null $store_id Идентификатор склада продавца
     *
     * @return $this
     */
    public function setStoreId($store_id)
    {


        /*if (is_null($store_id)) {
            throw new \InvalidArgumentException('non-nullable store_id cannot be null');
        }*/
        $this->container['store_id'] = $store_id;

        return $this;
    }

    /**
     * Gets delivery_service_id
     *
     * @return int|null
     */
    public function getDeliveryServiceId()
    {
        return $this->container['delivery_service_id'];
    }

    /**
     * Sets delivery_service_id
     *
     * @param int|null $delivery_service_id Идентификатор сервиса доставки
     *
     * @return $this
     */
    public function setDeliveryServiceId($delivery_service_id)
    {


        /*if (is_null($delivery_service_id)) {
            throw new \InvalidArgumentException('non-nullable delivery_service_id cannot be null');
        }*/
        $this->container['delivery_service_id'] = $delivery_service_id;

        return $this;
    }

    /**
     * Gets created_at
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     *
     * @param \DateTime|null $created_at Дата создания
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {


        /*if (is_null($created_at)) {
            throw new \InvalidArgumentException('non-nullable created_at cannot be null');
        }*/
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime|null $updated_at Дата обновления
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {


        /*if (is_null($updated_at)) {
            throw new \InvalidArgumentException('non-nullable updated_at cannot be null');
        }*/
        $this->container['updated_at'] = $updated_at;

        return $this;
    }

    /**
     * Gets status_at
     *
     * @return \DateTime|null
     */
    public function getStatusAt()
    {
        return $this->container['status_at'];
    }

    /**
     * Sets status_at
     *
     * @param \DateTime|null $status_at Дата установки статуса
     *
     * @return $this
     */
    public function setStatusAt($status_at)
    {


        /*if (is_null($status_at)) {
            throw new \InvalidArgumentException('non-nullable status_at cannot be null');
        }*/
        $this->container['status_at'] = $status_at;

        return $this;
    }

    /**
     * Gets is_problem_at
     *
     * @return \DateTime|null
     */
    public function getIsProblemAt()
    {
        return $this->container['is_problem_at'];
    }

    /**
     * Sets is_problem_at
     *
     * @param \DateTime|null $is_problem_at Дата установки флага проблемного груза
     *
     * @return $this
     */
    public function setIsProblemAt($is_problem_at)
    {


        /*if (is_null($is_problem_at)) {
            throw new \InvalidArgumentException('non-nullable is_problem_at cannot be null');
        }*/
        $this->container['is_problem_at'] = $is_problem_at;

        return $this;
    }

    /**
     * Gets is_canceled_at
     *
     * @return \DateTime|null
     */
    public function getIsCanceledAt()
    {
        return $this->container['is_canceled_at'];
    }

    /**
     * Sets is_canceled_at
     *
     * @param \DateTime|null $is_canceled_at Дата установки флага отмены груза
     *
     * @return $this
     */
    public function setIsCanceledAt($is_canceled_at)
    {


        /*if (is_null($is_canceled_at)) {
            throw new \InvalidArgumentException('non-nullable is_canceled_at cannot be null');
        }*/
        $this->container['is_canceled_at'] = $is_canceled_at;

        return $this;
    }

    /**
     * Gets error_xml_id
     *
     * @return string|null
     */
    public function getErrorXmlId()
    {
        return $this->container['error_xml_id'];
    }

    /**
     * Sets error_xml_id
     *
     * @param string|null $error_xml_id error_xml_id
     *
     * @return $this
     */
    public function setErrorXmlId($error_xml_id)
    {


        /*if (is_null($error_xml_id)) {
            throw new \InvalidArgumentException('non-nullable error_xml_id cannot be null');
        }*/
        $this->container['error_xml_id'] = $error_xml_id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


