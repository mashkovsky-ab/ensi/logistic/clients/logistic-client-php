<?php
/**
 * SearchDeliveryPricesFilterTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\LogisticClient;

use PHPUnit\Framework\TestCase;

/**
 * SearchDeliveryPricesFilterTest Class Doc Comment
 *
 * @category    Class
 * @description SearchDeliveryPricesFilter
 * @package     Ensi\LogisticClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SearchDeliveryPricesFilterTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SearchDeliveryPricesFilter"
     */
    public function testSearchDeliveryPricesFilter()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "federal_district_id"
     */
    public function testPropertyFederalDistrictId()
    {
    }

    /**
     * Test attribute "region_id"
     */
    public function testPropertyRegionId()
    {
    }

    /**
     * Test attribute "region_guid"
     */
    public function testPropertyRegionGuid()
    {
    }

    /**
     * Test attribute "delivery_service"
     */
    public function testPropertyDeliveryService()
    {
    }

    /**
     * Test attribute "delivery_method"
     */
    public function testPropertyDeliveryMethod()
    {
    }
}
