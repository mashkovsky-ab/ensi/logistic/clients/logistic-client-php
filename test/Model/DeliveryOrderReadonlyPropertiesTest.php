<?php
/**
 * DeliveryOrderReadonlyPropertiesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\LogisticClient;

use PHPUnit\Framework\TestCase;

/**
 * DeliveryOrderReadonlyPropertiesTest Class Doc Comment
 *
 * @category    Class
 * @description DeliveryOrderReadonlyProperties
 * @package     Ensi\LogisticClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class DeliveryOrderReadonlyPropertiesTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "DeliveryOrderReadonlyProperties"
     */
    public function testDeliveryOrderReadonlyProperties()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "delivery_id"
     */
    public function testPropertyDeliveryId()
    {
    }

    /**
     * Test attribute "delivery_service_id"
     */
    public function testPropertyDeliveryServiceId()
    {
    }

    /**
     * Test attribute "number"
     */
    public function testPropertyNumber()
    {
    }

    /**
     * Test attribute "external_id"
     */
    public function testPropertyExternalId()
    {
    }

    /**
     * Test attribute "error_external_id"
     */
    public function testPropertyErrorExternalId()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "status_at"
     */
    public function testPropertyStatusAt()
    {
    }

    /**
     * Test attribute "external_status"
     */
    public function testPropertyExternalStatus()
    {
    }

    /**
     * Test attribute "external_status_at"
     */
    public function testPropertyExternalStatusAt()
    {
    }

    /**
     * Test attribute "height"
     */
    public function testPropertyHeight()
    {
    }

    /**
     * Test attribute "length"
     */
    public function testPropertyLength()
    {
    }

    /**
     * Test attribute "width"
     */
    public function testPropertyWidth()
    {
    }

    /**
     * Test attribute "weight"
     */
    public function testPropertyWeight()
    {
    }

    /**
     * Test attribute "shipment_method"
     */
    public function testPropertyShipmentMethod()
    {
    }

    /**
     * Test attribute "delivery_method"
     */
    public function testPropertyDeliveryMethod()
    {
    }

    /**
     * Test attribute "tariff_id"
     */
    public function testPropertyTariffId()
    {
    }

    /**
     * Test attribute "delivery_date"
     */
    public function testPropertyDeliveryDate()
    {
    }

    /**
     * Test attribute "point_in_id"
     */
    public function testPropertyPointInId()
    {
    }

    /**
     * Test attribute "point_out_id"
     */
    public function testPropertyPointOutId()
    {
    }

    /**
     * Test attribute "shipment_time_start"
     */
    public function testPropertyShipmentTimeStart()
    {
    }

    /**
     * Test attribute "shipment_time_end"
     */
    public function testPropertyShipmentTimeEnd()
    {
    }

    /**
     * Test attribute "delivery_time_start"
     */
    public function testPropertyDeliveryTimeStart()
    {
    }

    /**
     * Test attribute "delivery_time_end"
     */
    public function testPropertyDeliveryTimeEnd()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "assessed_cost"
     */
    public function testPropertyAssessedCost()
    {
    }

    /**
     * Test attribute "delivery_cost"
     */
    public function testPropertyDeliveryCost()
    {
    }

    /**
     * Test attribute "delivery_cost_vat"
     */
    public function testPropertyDeliveryCostVat()
    {
    }

    /**
     * Test attribute "delivery_cost_pay"
     */
    public function testPropertyDeliveryCostPay()
    {
    }

    /**
     * Test attribute "cod_cost"
     */
    public function testPropertyCodCost()
    {
    }

    /**
     * Test attribute "is_delivery_payed_by_recipient"
     */
    public function testPropertyIsDeliveryPayedByRecipient()
    {
    }

    /**
     * Test attribute "sender_is_seller"
     */
    public function testPropertySenderIsSeller()
    {
    }

    /**
     * Test attribute "sender_inn"
     */
    public function testPropertySenderInn()
    {
    }

    /**
     * Test attribute "sender_address"
     */
    public function testPropertySenderAddress()
    {
    }

    /**
     * Test attribute "sender_company_name"
     */
    public function testPropertySenderCompanyName()
    {
    }

    /**
     * Test attribute "sender_contact_name"
     */
    public function testPropertySenderContactName()
    {
    }

    /**
     * Test attribute "sender_email"
     */
    public function testPropertySenderEmail()
    {
    }

    /**
     * Test attribute "sender_phone"
     */
    public function testPropertySenderPhone()
    {
    }

    /**
     * Test attribute "sender_comment"
     */
    public function testPropertySenderComment()
    {
    }

    /**
     * Test attribute "recipient_address"
     */
    public function testPropertyRecipientAddress()
    {
    }

    /**
     * Test attribute "recipient_company_name"
     */
    public function testPropertyRecipientCompanyName()
    {
    }

    /**
     * Test attribute "recipient_contact_name"
     */
    public function testPropertyRecipientContactName()
    {
    }

    /**
     * Test attribute "recipient_email"
     */
    public function testPropertyRecipientEmail()
    {
    }

    /**
     * Test attribute "recipient_phone"
     */
    public function testPropertyRecipientPhone()
    {
    }

    /**
     * Test attribute "recipient_comment"
     */
    public function testPropertyRecipientComment()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }
}
