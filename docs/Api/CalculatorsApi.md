# Ensi\LogisticClient\CalculatorsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculateDelivery**](CalculatorsApi.md#calculateDelivery) | **POST** /calculators/calculators:calculate-delivery | Раcсчитать сроки и стоимость доставки по заданным характеристикам



## calculateDelivery

> \Ensi\LogisticClient\Dto\CalculateDeliveryResponse calculateDelivery($calculate_delivery_request)

Раcсчитать сроки и стоимость доставки по заданным характеристикам

Раcсчитать сроки и стоимость доставки по заданным характеристикам

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CalculatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$calculate_delivery_request = new \Ensi\LogisticClient\Dto\CalculateDeliveryRequest(); // \Ensi\LogisticClient\Dto\CalculateDeliveryRequest | 

try {
    $result = $apiInstance->calculateDelivery($calculate_delivery_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CalculatorsApi->calculateDelivery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculate_delivery_request** | [**\Ensi\LogisticClient\Dto\CalculateDeliveryRequest**](../Model/CalculateDeliveryRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\CalculateDeliveryResponse**](../Model/CalculateDeliveryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

