# Ensi\LogisticClient\DeliveryServicesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPaymentMethodsToDeliveryService**](DeliveryServicesApi.md#addPaymentMethodsToDeliveryService) | **POST** /delivery-services/delivery-services/{id}:add-payment-methods | Добавление доступных способов оплаты службе доставки
[**createDeliveryServiceDocument**](DeliveryServicesApi.md#createDeliveryServiceDocument) | **POST** /delivery-services/delivery-service-documents | Создание объекта типа DeliveryServiceDocument
[**createDeliveryServiceManager**](DeliveryServicesApi.md#createDeliveryServiceManager) | **POST** /delivery-services/delivery-service-managers | Создание объекта типа DeliveryServiceManager
[**deleteDeliveryServiceDocument**](DeliveryServicesApi.md#deleteDeliveryServiceDocument) | **DELETE** /delivery-services/delivery-service-documents/{id} | Удаление объекта типа DeliveryServiceDocument
[**deleteDeliveryServiceDocumentFile**](DeliveryServicesApi.md#deleteDeliveryServiceDocumentFile) | **POST** /delivery-services/delivery-service-documents/{id}:delete-file | Удаление файла с документом службы доставки
[**deleteDeliveryServiceManager**](DeliveryServicesApi.md#deleteDeliveryServiceManager) | **DELETE** /delivery-services/delivery-service-managers/{id} | Удаление объекта типа DeliveryServiceManager
[**deletePaymentMethodFromDeliveryService**](DeliveryServicesApi.md#deletePaymentMethodFromDeliveryService) | **POST** /delivery-services/delivery-services/{id}:delete-payment-method | Удаление доступного способа оплаты у службы доставки
[**getDeliveryMethods**](DeliveryServicesApi.md#getDeliveryMethods) | **GET** /delivery-services/delivery-methods | Получение объекта типа DeliveryMethod
[**getDeliveryService**](DeliveryServicesApi.md#getDeliveryService) | **GET** /delivery-services/delivery-services/{id} | Получение объекта типа DeliveryService
[**getDeliveryServiceDocument**](DeliveryServicesApi.md#getDeliveryServiceDocument) | **GET** /delivery-services/delivery-service-documents/{id} | Получение объекта типа DeliveryServiceDocument
[**getDeliveryServiceManager**](DeliveryServicesApi.md#getDeliveryServiceManager) | **GET** /delivery-services/delivery-service-managers/{id} | Получение объекта типа DeliveryServiceManager
[**getDeliveryServiceStatuses**](DeliveryServicesApi.md#getDeliveryServiceStatuses) | **GET** /delivery-services/delivery-service-statuses | Получение объекта типа DeliveryServiceStatus
[**getShipmentMethods**](DeliveryServicesApi.md#getShipmentMethods) | **GET** /delivery-services/shipment-methods | Получение объекта типа ShipmentMethod
[**patchDeliveryService**](DeliveryServicesApi.md#patchDeliveryService) | **PATCH** /delivery-services/delivery-services/{id} | Обновления части полей объекта типа DeliveryService
[**patchDeliveryServiceDocument**](DeliveryServicesApi.md#patchDeliveryServiceDocument) | **PATCH** /delivery-services/delivery-service-documents/{id} | Обновления части полей объекта типа DeliveryServiceDocument
[**patchDeliveryServiceManager**](DeliveryServicesApi.md#patchDeliveryServiceManager) | **PATCH** /delivery-services/delivery-service-managers/{id} | Обновления части полей объекта типа DeliveryServiceManager
[**replaceDeliveryService**](DeliveryServicesApi.md#replaceDeliveryService) | **PUT** /delivery-services/delivery-services/{id} | Замена объекта типа DeliveryService
[**replaceDeliveryServiceDocument**](DeliveryServicesApi.md#replaceDeliveryServiceDocument) | **PUT** /delivery-services/delivery-service-documents/{id} | Замена объекта типа DeliveryServiceDocument
[**replaceDeliveryServiceManager**](DeliveryServicesApi.md#replaceDeliveryServiceManager) | **PUT** /delivery-services/delivery-service-managers/{id} | Замена объекта типа DeliveryServiceManager
[**searchDeliveryServiceDocuments**](DeliveryServicesApi.md#searchDeliveryServiceDocuments) | **POST** /delivery-services/delivery-service-documents:search | Поиск объектов типа DeliveryServiceDocument
[**searchDeliveryServiceManagers**](DeliveryServicesApi.md#searchDeliveryServiceManagers) | **POST** /delivery-services/delivery-service-managers:search | Поиск объектов типа DeliveryServiceManager
[**searchDeliveryServices**](DeliveryServicesApi.md#searchDeliveryServices) | **POST** /delivery-services/delivery-services:search | Поиск объектов типа DeliveryService
[**searchOneDeliveryService**](DeliveryServicesApi.md#searchOneDeliveryService) | **POST** /delivery-services/delivery-services:search-one | Поиск объекта типа DeliveryService
[**searchOneDeliveryServiceDocument**](DeliveryServicesApi.md#searchOneDeliveryServiceDocument) | **POST** /delivery-services/delivery-service-documents:search-one | Поиск объекта типа DeliveryServiceDocument
[**searchOneDeliveryServiceManager**](DeliveryServicesApi.md#searchOneDeliveryServiceManager) | **POST** /delivery-services/delivery-service-managers:search-one | Поиск объекта типа DeliveryServiceManager
[**uploadDeliveryServiceDocumentFile**](DeliveryServicesApi.md#uploadDeliveryServiceDocumentFile) | **POST** /delivery-services/delivery-service-documents/{id}:upload-file | Загрузка файла с документом службы доставки



## addPaymentMethodsToDeliveryService

> \Ensi\LogisticClient\Dto\EmptyDataResponse addPaymentMethodsToDeliveryService($id, $add_payment_methods_to_delivery_service_request)

Добавление доступных способов оплаты службе доставки

Добавление доступных способов оплаты службе доставки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$add_payment_methods_to_delivery_service_request = new \Ensi\LogisticClient\Dto\AddPaymentMethodsToDeliveryServiceRequest(); // \Ensi\LogisticClient\Dto\AddPaymentMethodsToDeliveryServiceRequest | 

try {
    $result = $apiInstance->addPaymentMethodsToDeliveryService($id, $add_payment_methods_to_delivery_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->addPaymentMethodsToDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **add_payment_methods_to_delivery_service_request** | [**\Ensi\LogisticClient\Dto\AddPaymentMethodsToDeliveryServiceRequest**](../Model/AddPaymentMethodsToDeliveryServiceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse createDeliveryServiceDocument($create_delivery_service_document_request)

Создание объекта типа DeliveryServiceDocument

Создание объекта типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_delivery_service_document_request = new \Ensi\LogisticClient\Dto\CreateDeliveryServiceDocumentRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryServiceDocumentRequest | 

try {
    $result = $apiInstance->createDeliveryServiceDocument($create_delivery_service_document_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->createDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_delivery_service_document_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryServiceDocumentRequest**](../Model/CreateDeliveryServiceDocumentRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDeliveryServiceManager

> \Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse createDeliveryServiceManager($create_delivery_service_manager_request)

Создание объекта типа DeliveryServiceManager

Создание объекта типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_delivery_service_manager_request = new \Ensi\LogisticClient\Dto\CreateDeliveryServiceManagerRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryServiceManagerRequest | 

try {
    $result = $apiInstance->createDeliveryServiceManager($create_delivery_service_manager_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->createDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_delivery_service_manager_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryServiceManagerRequest**](../Model/CreateDeliveryServiceManagerRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse**](../Model/DeliveryServiceManagerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryServiceDocument($id)

Удаление объекта типа DeliveryServiceDocument

Удаление объекта типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDeliveryServiceDocument($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->deleteDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryServiceDocumentFile

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse deleteDeliveryServiceDocumentFile($id)

Удаление файла с документом службы доставки

Удаление файла с документом службы доставки из базы и файловой системы

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDeliveryServiceDocumentFile($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->deleteDeliveryServiceDocumentFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryServiceManager

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryServiceManager($id)

Удаление объекта типа DeliveryServiceManager

Удаление объекта типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDeliveryServiceManager($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->deleteDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePaymentMethodFromDeliveryService

> \Ensi\LogisticClient\Dto\EmptyDataResponse deletePaymentMethodFromDeliveryService($id, $delete_payment_method_from_delivery_service_request)

Удаление доступного способа оплаты у службы доставки

Удаление доступного способа оплаты у службы доставки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delete_payment_method_from_delivery_service_request = new \Ensi\LogisticClient\Dto\DeletePaymentMethodFromDeliveryServiceRequest(); // \Ensi\LogisticClient\Dto\DeletePaymentMethodFromDeliveryServiceRequest | 

try {
    $result = $apiInstance->deletePaymentMethodFromDeliveryService($id, $delete_payment_method_from_delivery_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->deletePaymentMethodFromDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delete_payment_method_from_delivery_service_request** | [**\Ensi\LogisticClient\Dto\DeletePaymentMethodFromDeliveryServiceRequest**](../Model/DeletePaymentMethodFromDeliveryServiceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryMethods

> \Ensi\LogisticClient\Dto\GetDeliveryMethodsResponse getDeliveryMethods()

Получение объекта типа DeliveryMethod

Получение объекта типа DeliveryMethod

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDeliveryMethods();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getDeliveryMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\LogisticClient\Dto\GetDeliveryMethodsResponse**](../Model/GetDeliveryMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryService

> \Ensi\LogisticClient\Dto\DeliveryServiceResponse getDeliveryService($id, $include)

Получение объекта типа DeliveryService

Получение объекта типа DeliveryService

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDeliveryService($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceResponse**](../Model/DeliveryServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse getDeliveryServiceDocument($id, $include)

Получение объекта типа DeliveryServiceDocument

Получение объекта типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDeliveryServiceDocument($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryServiceManager

> \Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse getDeliveryServiceManager($id, $include)

Получение объекта типа DeliveryServiceManager

Получение объекта типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDeliveryServiceManager($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse**](../Model/DeliveryServiceManagerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryServiceStatuses

> \Ensi\LogisticClient\Dto\GetDeliveryServiceStatusesResponse getDeliveryServiceStatuses()

Получение объекта типа DeliveryServiceStatus

Получение объекта типа DeliveryServiceStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDeliveryServiceStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getDeliveryServiceStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\LogisticClient\Dto\GetDeliveryServiceStatusesResponse**](../Model/GetDeliveryServiceStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getShipmentMethods

> \Ensi\LogisticClient\Dto\GetShipmentMethodsResponse getShipmentMethods()

Получение объекта типа ShipmentMethod

Получение объекта типа ShipmentMethod

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getShipmentMethods();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->getShipmentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\LogisticClient\Dto\GetShipmentMethodsResponse**](../Model/GetShipmentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryService

> \Ensi\LogisticClient\Dto\DeliveryServiceResponse patchDeliveryService($id, $patch_delivery_service_request)

Обновления части полей объекта типа DeliveryService

Обновления части полей объекта типа DeliveryService

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_service_request = new \Ensi\LogisticClient\Dto\PatchDeliveryServiceRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryServiceRequest | 

try {
    $result = $apiInstance->patchDeliveryService($id, $patch_delivery_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->patchDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_service_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryServiceRequest**](../Model/PatchDeliveryServiceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceResponse**](../Model/DeliveryServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse patchDeliveryServiceDocument($id, $patch_delivery_service_document_request)

Обновления части полей объекта типа DeliveryServiceDocument

Обновления части полей объекта типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_service_document_request = new \Ensi\LogisticClient\Dto\PatchDeliveryServiceDocumentRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryServiceDocumentRequest | 

try {
    $result = $apiInstance->patchDeliveryServiceDocument($id, $patch_delivery_service_document_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->patchDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_service_document_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryServiceDocumentRequest**](../Model/PatchDeliveryServiceDocumentRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryServiceManager

> \Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse patchDeliveryServiceManager($id, $patch_delivery_service_manager_request)

Обновления части полей объекта типа DeliveryServiceManager

Обновления части полей объекта типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_service_manager_request = new \Ensi\LogisticClient\Dto\PatchDeliveryServiceManagerRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryServiceManagerRequest | 

try {
    $result = $apiInstance->patchDeliveryServiceManager($id, $patch_delivery_service_manager_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->patchDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_service_manager_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryServiceManagerRequest**](../Model/PatchDeliveryServiceManagerRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse**](../Model/DeliveryServiceManagerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceDeliveryService

> \Ensi\LogisticClient\Dto\DeliveryServiceResponse replaceDeliveryService($id, $replace_delivery_service_request)

Замена объекта типа DeliveryService

Замена объекта типа DeliveryService

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_delivery_service_request = new \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceRequest(); // \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceRequest | 

try {
    $result = $apiInstance->replaceDeliveryService($id, $replace_delivery_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->replaceDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_delivery_service_request** | [**\Ensi\LogisticClient\Dto\ReplaceDeliveryServiceRequest**](../Model/ReplaceDeliveryServiceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceResponse**](../Model/DeliveryServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse replaceDeliveryServiceDocument($id, $replace_delivery_service_document_request)

Замена объекта типа DeliveryServiceDocument

Замена объекта типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_delivery_service_document_request = new \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceDocumentRequest(); // \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceDocumentRequest | 

try {
    $result = $apiInstance->replaceDeliveryServiceDocument($id, $replace_delivery_service_document_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->replaceDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_delivery_service_document_request** | [**\Ensi\LogisticClient\Dto\ReplaceDeliveryServiceDocumentRequest**](../Model/ReplaceDeliveryServiceDocumentRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceDeliveryServiceManager

> \Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse replaceDeliveryServiceManager($id, $replace_delivery_service_manager_request)

Замена объекта типа DeliveryServiceManager

Замена объекта типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_delivery_service_manager_request = new \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceManagerRequest(); // \Ensi\LogisticClient\Dto\ReplaceDeliveryServiceManagerRequest | 

try {
    $result = $apiInstance->replaceDeliveryServiceManager($id, $replace_delivery_service_manager_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->replaceDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_delivery_service_manager_request** | [**\Ensi\LogisticClient\Dto\ReplaceDeliveryServiceManagerRequest**](../Model/ReplaceDeliveryServiceManagerRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse**](../Model/DeliveryServiceManagerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryServiceDocuments

> \Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsResponse searchDeliveryServiceDocuments($search_delivery_service_documents_request)

Поиск объектов типа DeliveryServiceDocument

Поиск объектов типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_service_documents_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest | 

try {
    $result = $apiInstance->searchDeliveryServiceDocuments($search_delivery_service_documents_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchDeliveryServiceDocuments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_service_documents_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest**](../Model/SearchDeliveryServiceDocumentsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsResponse**](../Model/SearchDeliveryServiceDocumentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryServiceManagers

> \Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersResponse searchDeliveryServiceManagers($search_delivery_service_managers_request)

Поиск объектов типа DeliveryServiceManager

Поиск объектов типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_service_managers_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest | 

try {
    $result = $apiInstance->searchDeliveryServiceManagers($search_delivery_service_managers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchDeliveryServiceManagers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_service_managers_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest**](../Model/SearchDeliveryServiceManagersRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersResponse**](../Model/SearchDeliveryServiceManagersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryServices

> \Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse searchDeliveryServices($search_delivery_services_request)

Поиск объектов типа DeliveryService

Поиск объектов типа DeliveryService

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_services_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest | 

try {
    $result = $apiInstance->searchDeliveryServices($search_delivery_services_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchDeliveryServices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_services_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest**](../Model/SearchDeliveryServicesRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse**](../Model/SearchDeliveryServicesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryService

> \Ensi\LogisticClient\Dto\DeliveryServiceResponse searchOneDeliveryService($search_delivery_services_request)

Поиск объекта типа DeliveryService

Поиск объектов типа DeliveryService

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_services_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest | 

try {
    $result = $apiInstance->searchOneDeliveryService($search_delivery_services_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchOneDeliveryService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_services_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest**](../Model/SearchDeliveryServicesRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceResponse**](../Model/DeliveryServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryServiceDocument

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse searchOneDeliveryServiceDocument($search_delivery_service_documents_request)

Поиск объекта типа DeliveryServiceDocument

Поиск объектов типа DeliveryServiceDocument

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_service_documents_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest | 

try {
    $result = $apiInstance->searchOneDeliveryServiceDocument($search_delivery_service_documents_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchOneDeliveryServiceDocument: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_service_documents_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest**](../Model/SearchDeliveryServiceDocumentsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryServiceManager

> \Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse searchOneDeliveryServiceManager($search_delivery_service_managers_request)

Поиск объекта типа DeliveryServiceManager

Поиск объектов типа DeliveryServiceManager

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_service_managers_request = new \Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest | 

try {
    $result = $apiInstance->searchOneDeliveryServiceManager($search_delivery_service_managers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->searchOneDeliveryServiceManager: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_service_managers_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest**](../Model/SearchDeliveryServiceManagersRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse**](../Model/DeliveryServiceManagerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadDeliveryServiceDocumentFile

> \Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse uploadDeliveryServiceDocumentFile($id, $file, $name)

Загрузка файла с документом службы доставки

Загрузка файла с документом службы доставки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл
$name = 'name_example'; // string | Имя файла

try {
    $result = $apiInstance->uploadDeliveryServiceDocumentFile($id, $file, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryServicesApi->uploadDeliveryServiceDocumentFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл |
 **name** | **string**| Имя файла | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse**](../Model/DeliveryServiceDocumentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

