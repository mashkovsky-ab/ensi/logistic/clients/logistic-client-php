# # SearchDeliveryPricesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\LogisticClient\Dto\DeliveryPrice[]**](DeliveryPrice.md) |  | 
**meta** | [**\Ensi\LogisticClient\Dto\SearchCargoOrdersResponseMeta**](SearchCargoOrdersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


