# # CalculationDelivery

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | **int** |  | 
**dt** | **int** | Delivery Time - время доставки в днях, которое отдаёт ЛО | 
**pdd** | [**\DateTime**](\DateTime.md) | Planned Delivery Date - плановая дата, начиная с которой отправление может быть доставлено клиенту | 
**shipments** | [**\Ensi\LogisticClient\Dto\CalculationShipment[]**](CalculationShipment.md) | Отправления, которые входят в доставку | 
**tariffs** | [**\Ensi\LogisticClient\Dto\CalculationTariff[]**](CalculationTariff.md) | Тарифы на доставку | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


