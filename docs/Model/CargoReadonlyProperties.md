# # CargoReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**seller_id** | **int** | Идентификатор продавца | [optional] 
**store_id** | **int** | Идентификатор склада продавца | [optional] 
**delivery_service_id** | **int** | Идентификатор сервиса доставки | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**status_at** | [**\DateTime**](\DateTime.md) | Дата установки статуса | [optional] 
**is_problem_at** | [**\DateTime**](\DateTime.md) | Дата установки флага проблемного груза | [optional] 
**is_canceled_at** | [**\DateTime**](\DateTime.md) | Дата установки флага отмены груза | [optional] 
**error_xml_id** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


