# # CalculationStore

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_id** | **int** | Id склада продавца | 
**city_guid** | **string** | Id города склада в базе ФИАС | 
**working** | [**\Ensi\LogisticClient\Dto\CalculationStoreWorking[]**](CalculationStoreWorking.md) |  | [optional] 
**pickup_times** | [**\Ensi\LogisticClient\Dto\CalculationStorePickupTime[]**](CalculationStorePickupTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


