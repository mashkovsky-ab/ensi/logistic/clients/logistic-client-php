# # SearchPointsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор пункта | [optional] 
**delivery_service** | **int** |  | [optional] 
**type** | **int** |  | [optional] 
**name** | **string** | Название пункта | [optional] 
**external_id** | **string** | Id пункта у службы доставки | [optional] 
**has_payment_card** | **bool** | Возможна ли оплата картой | [optional] 
**email** | **string** | E-mail | [optional] 
**phone** | **string** | Телефон | [optional] 
**active** | **bool** | Активность | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


