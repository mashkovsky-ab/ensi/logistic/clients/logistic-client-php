# # CargoFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** |  | [optional] 
**is_problem** | **bool** | Флаг, что у груза проблемы при отгрузке | [optional] 
**is_canceled** | **bool** | Флаг, что груз отменен | [optional] 
**width** | **float** | Ширина | [optional] 
**height** | **float** | Высота | [optional] 
**length** | **float** | Длина | [optional] 
**weight** | **float** | Вес | [optional] 
**shipping_problem_comment** | **string** | Последнее сообщение мерчанта о проблеме с отгрузкой | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


