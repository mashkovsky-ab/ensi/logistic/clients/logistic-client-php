# # CalculationDeliveryMethod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**can_merge** | **bool** | Можно ли консолидировать все доставки в одну | 
**delivery_cost** | **int** | Стоимость доставки для клиента (в копейках) | 
**delivery_method_id** | **int** |  | 
**deliveries** | [**\Ensi\LogisticClient\Dto\CalculationDelivery[]**](CalculationDelivery.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


