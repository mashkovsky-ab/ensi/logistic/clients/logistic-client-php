# # CalculationStoreWorking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **int** | День недели (1-7) | 
**working_start_time** | **string** | Время начала работы склада | 
**working_end_time** | **string** | Время окончания работы склада | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


