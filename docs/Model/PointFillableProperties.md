# # PointFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | **int** |  | [optional] 
**type** | **int** |  | [optional] 
**name** | **string** | Название пункта | [optional] 
**external_id** | **string** | Id пункта у службы доставки | [optional] 
**apiship_external_id** | **string** | Id пункта у ApiShip | [optional] 
**has_payment_card** | **bool** | Возможна ли оплата картой | [optional] 
**address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 
**email** | **string** | E-mail | [optional] 
**phone** | **string** | Телефон | [optional] 
**timetable** | **string** | График работы | [optional] 
**active** | **bool** | Активность | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


