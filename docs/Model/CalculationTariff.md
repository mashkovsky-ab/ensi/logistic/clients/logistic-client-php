# # CalculationTariff

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **int** | Id тарифа в службе доставки | 
**name** | **string** | Наименование тарифа | 
**delivery_service_cost** | **int** | Стоимость доставки от службы доставки (в копейках) | 
**days_min** | **int** | Минимальное количество дней на доставку | [optional] 
**days_max** | **int** | Максимальное количество дней на доставку | [optional] 
**available_dates** | [**\Ensi\LogisticClient\Dto\CalculationAvailableDate[]**](CalculationAvailableDate.md) | Доступные даты для доставки | [optional] 
**point_ids** | **int[]** | Список Id точек выдачи заказа | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


