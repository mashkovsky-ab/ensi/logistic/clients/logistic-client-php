# # SearchDeliveryOrdersFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор заказа на доставку | [optional] 
**delivery_id** | **int** | Id Отправления в OMS | [optional] 
**delivery_service_id** | **int** |  | [optional] 
**number** | **string** | Номер заказа на доставку внутри платформы (совпадает с номером Отправления в OMS) | [optional] 
**status** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


