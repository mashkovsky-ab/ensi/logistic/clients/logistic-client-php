# # CalculateDeliveryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city_guid_to** | **string** | Id города получателя в базе ФИАС | 
**basket_items** | [**\Ensi\LogisticClient\Dto\CalculationBasketItem[]**](CalculationBasketItem.md) | Содержимое корзины для доставки | 
**stores** | [**\Ensi\LogisticClient\Dto\CalculationStore[]**](CalculationStore.md) | Информация о складах, с которых идет доставка | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


