# # DeliveryPriceIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**federal_district** | [**\Ensi\LogisticClient\Dto\FederalDistrict**](FederalDistrict.md) |  | [optional] 
**region** | [**\Ensi\LogisticClient\Dto\Region**](Region.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


