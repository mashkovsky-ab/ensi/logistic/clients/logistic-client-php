# # SearchCargoOrdersFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**timeslot_id** | **string** | Идентификатор таймслота | [optional] 
**status** | **int** | Статус | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


