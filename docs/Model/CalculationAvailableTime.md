# # CalculationAvailableTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Код интервала доставки | 
**from** | **int** | Начальный час доставки «С» | 
**to** | **int** | Конечный час доставки «ДО» | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


