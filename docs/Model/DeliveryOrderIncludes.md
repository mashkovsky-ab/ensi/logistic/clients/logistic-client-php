# # DeliveryOrderIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**places** | [**\Ensi\LogisticClient\Dto\DeliveryOrderPlace[]**](DeliveryOrderPlace.md) | Места в заказе на доставку | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


