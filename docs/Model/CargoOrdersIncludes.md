# # CargoOrdersIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cargo** | [**\Ensi\LogisticClient\Dto\Cargo**](Cargo.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


