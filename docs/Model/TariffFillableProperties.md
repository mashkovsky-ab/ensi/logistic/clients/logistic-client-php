# # TariffFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | **int** |  | [optional] 
**delivery_method** | **int** |  | [optional] 
**shipment_method** | **int** |  | [optional] 
**name** | **string** | Название | [optional] 
**external_id** | **string** | Id тарифа у службы доставки | [optional] 
**apiship_external_id** | **string** | Id тарифа у ApiShip | [optional] 
**weight_min** | **int** | Минимальное ограничение по весу для тарифа (граммы) | [optional] 
**weight_max** | **int** | Максимальное ограничение по весу для тарифа (граммы) | [optional] 
**description** | **string** | Описание | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


