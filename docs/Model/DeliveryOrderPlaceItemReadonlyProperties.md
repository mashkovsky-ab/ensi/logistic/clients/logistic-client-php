# # DeliveryOrderPlaceItemReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор товара места (коробки) в заказе на доставку | [optional] 
**delivery_order_place_id** | **int** | Идентификатор места (коробки) в заказе на доставку | [optional] 
**vendor_code** | **string** | Артикул товара | [optional] 
**barcode** | **string** | Штрихкод на товаре | [optional] 
**name** | **string** | Наименование товара | [optional] 
**qty** | **float** | Кол-во товара | [optional] 
**qty_delivered** | **float** | Заполняется только при частичной доставке и показывает сколько вложимых выкуплено | [optional] 
**height** | **int** | Высота единицы товара (в мм) | [optional] 
**length** | **int** | Длина единицы товара (в мм) | [optional] 
**width** | **int** | Ширина единицы товара (в мм) | [optional] 
**weight** | **int** | Вес единицы товара (в граммах) | [optional] 
**assessed_cost** | **int** | Оценочная стоимость единицы товара (в копейках) | [optional] 
**cost** | **int** | Стоимость единицы товара с учетом НДС (в копейках) | [optional] 
**cost_vat** | **int** | Процентная ставка НДС | [optional] 
**price** | **int** | Цена единицы товара к оплате с учетом НДС (в копейках) | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


