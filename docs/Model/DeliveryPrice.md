# # DeliveryPrice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор цены доставки | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**federal_district_id** | **int** | Id федерального округа | [optional] 
**region_id** | **int** | Id региона | [optional] 
**region_guid** | **string** | Id ФИАС региона | [optional] 
**delivery_service** | **int** |  | [optional] 
**delivery_method** | **int** |  | [optional] 
**price** | **int** | Цена в копейках | [optional] 
**federal_district** | [**\Ensi\LogisticClient\Dto\FederalDistrict**](FederalDistrict.md) |  | [optional] 
**region** | [**\Ensi\LogisticClient\Dto\Region**](Region.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


