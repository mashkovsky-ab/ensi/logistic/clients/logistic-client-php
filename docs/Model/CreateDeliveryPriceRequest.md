# # CreateDeliveryPriceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**federal_district_id** | **int** | Id федерального округа | [optional] 
**region_id** | **int** | Id региона | [optional] 
**region_guid** | **string** | Id ФИАС региона | [optional] 
**delivery_service** | **int** |  | [optional] 
**delivery_method** | **int** |  | [optional] 
**price** | **int** | Цена в копейках | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


