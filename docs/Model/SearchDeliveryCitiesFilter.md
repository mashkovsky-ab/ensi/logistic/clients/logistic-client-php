# # SearchDeliveryCitiesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | **int** |  | [optional] 
**city_id** | **int** | id населенного пункта | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


