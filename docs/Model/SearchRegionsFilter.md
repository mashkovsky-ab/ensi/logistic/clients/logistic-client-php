# # SearchRegionsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор региона | [optional] 
**federal_district_id** | **int** | Id федерального округа | [optional] 
**name** | **string** | Название | [optional] 
**guid** | **string** | Id ФИАС | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


