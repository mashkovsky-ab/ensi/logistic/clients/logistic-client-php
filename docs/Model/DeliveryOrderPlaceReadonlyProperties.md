# # DeliveryOrderPlaceReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор места в заказе на доставку | [optional] 
**delivery_order_id** | **int** | Id заказа на доставку | [optional] 
**number** | **string** | Номер места в информационной системе клиента | [optional] 
**barcode** | **string** | Штрихкод места | [optional] 
**height** | **int** | Высота места (в мм) | [optional] 
**length** | **int** | Длина места (в мм) | [optional] 
**width** | **int** | Ширина места (в мм) | [optional] 
**weight** | **int** | Вес места (в граммах) | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


