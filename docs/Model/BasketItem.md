# # BasketItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | ID оффера | 
**qty** | **int** | Кол-во оффера | 
**weight** | **float** | Вес (в кг) | 
**width** | **int** | Ширина (в мм) | 
**height** | **int** | Высота (в мм) | 
**length** | **int** | Длина (в мм) | 
**is_explosive** | **bool** | Cодержит взрывоопасные элементы? | [optional] 
**store_id** | **int** | Id склада продавца | 
**seller_id** | **int** | Id продавца | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


