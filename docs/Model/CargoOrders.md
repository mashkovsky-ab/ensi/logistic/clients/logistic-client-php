# # CargoOrders

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**cdek_intake_number** | **string** | Номер заявки СДЭК на вызов курьера | [optional] 
**external_id** | **string** | Номер заявки во внешней системе | [optional] 
**error_external_id** | **string** | Текст последней ошибки при создании заявки на вызов курьера для забора груза в службе доставки | [optional] 
**status** | **int** |  | [optional] 
**cargo_id** | **int** | Идентификатор груза | [optional] 
**timeslot_id** | **string** | Идентификатор таймслота | [optional] 
**timeslot_from** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**timeslot_to** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**date** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**cargo** | [**\Ensi\LogisticClient\Dto\Cargo**](Cargo.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


