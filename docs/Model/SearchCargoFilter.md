# # SearchCargoFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_id** | **int** | Идентификатор продавца | [optional] 
**store_id** | **int** | Идентификатор склада продавца | [optional] 
**status** | **int** | Статус | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


