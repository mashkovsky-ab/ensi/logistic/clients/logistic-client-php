# # CalculateDeliveryResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**methods** | [**\Ensi\LogisticClient\Dto\CalculationDeliveryMethod[]**](CalculationDeliveryMethod.md) | Способы получения товара | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


