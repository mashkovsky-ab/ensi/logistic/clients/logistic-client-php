# # MetroStationFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metro_line_id** | **int** | Id линии метро | [optional] 
**name** | **string** | Название | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


