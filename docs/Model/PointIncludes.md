# # PointIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metro_stations** | [**\Ensi\LogisticClient\Dto\MetroStation[]**](MetroStation.md) | Станции метро | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


