# # MetroStation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор станции метро | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**metro_line_id** | **int** | Id линии метро | [optional] 
**name** | **string** | Название | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 
**metro_line** | [**\Ensi\LogisticClient\Dto\MetroLine[]**](MetroLine.md) | Линия метро | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


