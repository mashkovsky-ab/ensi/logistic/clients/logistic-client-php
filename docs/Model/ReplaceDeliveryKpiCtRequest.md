# # ReplaceDeliveryKpiCtRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ct** | **int** | Confirmation Time - время перехода Отправления из статуса “Ожидает подтверждения” в статус “На комплектации” (мин) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


