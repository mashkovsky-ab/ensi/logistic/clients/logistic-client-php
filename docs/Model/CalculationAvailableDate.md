# # CalculationAvailableDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | Дата доставки | 
**available_times** | [**\Ensi\LogisticClient\Dto\CalculationAvailableTime[]**](CalculationAvailableTime.md) | Доступные интервалы времени доставки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


