# # DeliveryCity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**delivery_service** | **int** |  | [optional] 
**city_id** | **int** | id населенного пункта | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 
**payload** | **map[string,string]** |  | [optional] 
**city** | [**\Ensi\LogisticClient\Dto\City**](City.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


