# # CalculationShipment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_id** | **int** | Id продавца | 
**store_id** | **int** | Id склада отправления | 
**weight** | **float** | Вес отправления (в граммах) | 
**width** | **int** | Ширина отправления (в см) | 
**height** | **int** | Высота отправления (в см) | 
**length** | **int** | Длина отправления (в см) | 
**psd** | [**\DateTime**](\DateTime.md) | Planned Shipment Date - плановая дата и время, когда отправление должно быть собрано (получит статус \&quot;Готово к отгрузке\&quot;) | 
**items** | [**\Ensi\LogisticClient\Dto\CalculationShipmentItem[]**](CalculationShipmentItem.md) | Товары отправления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


