# # DeliveryServiceManager

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор менеджера | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**delivery_service_id** | **int** | Id службы доставки | [optional] 
**name** | **string** | ФИО менеджера | [optional] 
**phone** | **string** | Телефон менеджера | [optional] 
**email** | **string** | E-mail менеджера | [optional] 
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


