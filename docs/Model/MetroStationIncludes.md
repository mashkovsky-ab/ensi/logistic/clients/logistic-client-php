# # MetroStationIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metro_line** | [**\Ensi\LogisticClient\Dto\MetroLine[]**](MetroLine.md) | Линия метро | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


