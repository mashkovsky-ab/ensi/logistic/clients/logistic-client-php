# # SearchTariffsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор тарифа | [optional] 
**delivery_service** | **int** |  | [optional] 
**delivery_method** | **int** |  | [optional] 
**shipment_method** | **int** |  | [optional] 
**name** | **string** | Название | [optional] 
**external_id** | **string** | Id тарифа у службы доставки | [optional] 
**description** | **string** | Описание | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


