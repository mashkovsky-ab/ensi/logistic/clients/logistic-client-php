# # ReplaceDeliveryKpiPptRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ppt** | **int** | Planned Processing Time - плановое время для прохождения Отправлением статусов от “На комплектации” до “Готов к передаче ЛО” (мин) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


